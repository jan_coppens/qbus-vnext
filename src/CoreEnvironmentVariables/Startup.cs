﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace QbusVnext
{
    public class Startup
    {

        public Startup()
        {}

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var builder = new ConfigurationBuilder();
            builder.AddEnvironmentVariables();
            var configuration = builder.Build();

            services.AddInstance(typeof(IConfigurationRoot), configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IConfigurationRoot configuration)
        {
            Console.WriteLine("Configuring application...");

            app.Use(async (context, next) =>
            {
                await Task.Run(() =>
                {
                    Console.WriteLine($"Request from '{context.Request.Headers["User-Agent"]}'");
                    return next.Invoke();
                });
            });

            app.UseDeveloperExceptionPage();
            app.UseWelcomePage("/welcome");

            app.Run(async (context) =>
            {
                context.Response.ContentType = "text/html";
                await PrintConfigurationRoot(configuration, context);
            });

            Console.WriteLine("Application ready!");
        }

        // Entry point for the application.
        public static void Main(string[] args) => WebApplication.Run<Startup>(args);

        async private Task PrintConfigurationRoot(IConfigurationRoot configurationRoot, HttpContext context)
        {
            await context.Response.WriteAsync("<ul>");

            foreach (var child in configurationRoot.GetChildren())
            {
                await PrintConfigurationSection(child, context);
            }

            await context.Response.WriteAsync("</ul>");
        }

        async private Task PrintConfigurationSection(IConfigurationSection configurationSection, HttpContext context)
        {
            await context.Response.WriteAsync($"<li>'{configurationSection.Key}' = '{configurationSection.Value}' ({configurationSection.Path})</li>");

            foreach (var child in configurationSection.GetChildren())
            {
                await PrintConfigurationSection(child, context);
            }
        }
    }
}
